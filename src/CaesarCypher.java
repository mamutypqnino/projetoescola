
public class CaesarCypher extends Cypher{
	 private int key;
	 
	 public CaesarCypher(int key) {
		 this.key = key;
		 replacements = generateReplacementsTable();
	 }
	 
	 
	 
	 public char[] generateReplacementsTable(){
		char[] replacementsAux = new char[alphabet.length];
		int keyAux;
		for (int j = 0; j < alphabet.length; j++) {
			if (j + key < alphabet.length) {
				replacementsAux[j] = alphabet[j + key];
			}else{
				
				keyAux = key + j;
				while(keyAux > alphabet.length - 1) {
					keyAux -= alphabet.length;
				}
				replacementsAux[j] = alphabet[keyAux];
			}
		}
		return replacementsAux;
	 }
}
