/**
 * Esta classe serve como "molde" �s classes CaesarCypher e RandomCypher.
 */
public class Cypher
{
    protected final char[] alphabet = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    protected char[] replacements;

    /**
     * Os m�todos abaixo ser�o implementados nas subclasses CaesarCypher e RandomCypher.
     */
    public String encode(String input) {
    	// Variavel que ira guardar o texto crifrado
        StringBuilder textCypher = new StringBuilder();
     
        // Criptografa cada caracter por vez 
        for(int c=0; c < input.length(); c++){
           for (int i = 0; i < alphabet.length; i++) {
        	   if(input.charAt(c) == alphabet[i]){
        		   textCypher.append(replacements[i]);
        	   }
           }
           if(input.charAt(c) == ' '){
    		   textCypher.append(input.charAt(c));
    	   }
		}
           
        return textCypher.toString();
    }

    public String decode(String input) {
    	// Variavel que ira guardar o texto crifrado
        StringBuilder textCypher = new StringBuilder();
     
        // Criptografa cada caracter por vez 
        for(int c=0; c < input.length(); c++){
           for (int i = 0; i < replacements.length; i++) {
        	   if(input.charAt(c) == replacements[i]){
        		   textCypher.append(alphabet[i]);
        	   }
           }
           if(input.charAt(c) == ' '){
    		   textCypher.append(input.charAt(c));
    	   }
		}
           
        return textCypher.toString();
    }

    public char[] generateReplacementsTable() {
        return null;
    }
    
    public void writeFile(String content) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(outputFilePath));
        buffWrite.append(content);
        buffWrite.close();
    }

 }