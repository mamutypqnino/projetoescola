
public class RandomCypher extends Cypher{
	  private String key;
	  
	  public RandomCypher(String key) {
		  this.key = key;
		  replacements = generateReplacementsTable();
	  }
	 
	 public char[] generateReplacementsTable(){
		char[] replacementsAux = new char[alphabet.length];
		char c;
		int arrayInvertido;
		
		StringBuilder keyAux = new StringBuilder(key);  
        
		for(int i=0;i<keyAux.length();i++){  
		   c = keyAux.charAt(i);  
		   if(c==' '){  
		     continue;  
		   }  
		   int index = keyAux.lastIndexOf(String.valueOf(c));  
		              
		   if(index!=i){  
			   keyAux.deleteCharAt(index);  
		      i = 0;  
		   }  
		} 
		
		arrayInvertido = replacementsAux.length - 1 ;
		for (int i = 0; i < replacementsAux.length; i++) {
			if (i < keyAux.length()) {
				replacementsAux[i] = keyAux.charAt(i);
				continue;
			}
			for (int j = 0; j < replacementsAux.length; j++) {
				if (alphabet[arrayInvertido] == replacementsAux[j]) {
					arrayInvertido--;
					break;
				}
			}
			replacementsAux[i] = alphabet[arrayInvertido];
			arrayInvertido--;
		}
		
		return replacementsAux;
	 }
  
}
