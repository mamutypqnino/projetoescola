import java.io.File;
import java.util.Scanner;

public class RunCypher {

    private static String inputFilePath;
    private static String outputFilePath;
    private static int key;
    private static char option;

    /**
     * @param args
     */
    public static void main(String[] args) {
    	 option = args[0].charAt(1);
         key = Integer.valueOf(args[1]);
         inputFilePath = args[2];
         outputFilePath = args[3];

    	  try {
              String text = new Scanner(new File(inputFilePath)).useDelimiter("\\Z").next();
              String output = "";
              String [] words = text.split(" ");

              for (String word : words) {
                  output += decode(word)+" ";
              }

              writeFile(output);
          } catch (IOException e) {
              System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
          }
    	  
    		
       
        /*CaesarCypher caesarCypher = new CaesarCypher(key);
        caesarCypher.setInputFilePath(inputFilePath);
        caesarCypher.setOutputFilePath(outputFilePath);

        if (option == 'e') {
            caesarCypher.encode();
        } else if (option == 'd') {
            caesarCypher.decode();
        }*/
    	



    }
}
