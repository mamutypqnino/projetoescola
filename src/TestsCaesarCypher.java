
public class TestsCaesarCypher {
	
	  public static void main(String[] args) {
	        Cypher c1 = new CaesarCypher(3);
	        Cypher c2 = new CaesarCypher(33);
	        Cypher c3 = new CaesarCypher(13);

	        assert c1.encode("ola").equals("rod");
	        
	        assert c1.decode(c1.encode("ola")).equals("ola");
	        
	        assert c1.decode(c1.encode("ola mundo")).equals("ola mundo");
	        assert c1.encode("ola mundo").equals("rod pxqgr");
	 
	        
	        assert c2.decode(c2.encode("hello")).equals("hello");

	        assert c3.encode("hello").equals("uryyb");
	        assert c3.decode(c3.encode("hello")).equals("hello");

	        Cypher rc = new RandomCypher("hello");
	        Cypher rc2 = new RandomCypher("ola");
	       
	        assert rc.encode("hi julius caesar").equals("wv ugsvgj lhzjhk");
	        assert rc.decode(rc.encode("ola")).equals("ola");

	        assert rc2.encode("hi julius caesar").equals("vu tgrugi aoyioj");
	        assert rc2.decode(rc2.encode("hello")).equals("hello");
	    }
}
